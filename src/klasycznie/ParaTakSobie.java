package klasycznie;

public class ParaTakSobie {
    private Object lewy;
    private Object prawy;

    public ParaTakSobie(Object lewy, Object prawy) {
        this.lewy = lewy;
        this.prawy = prawy;
    }

    public Object getLewy() {
        return lewy;
    }

    public void setLewy(Object lewy) {
        this.lewy = lewy;
    }

    public Object getPrawy() {
        return prawy;
    }

    public void setPrawy(Object prawy) {
        this.prawy = prawy;
    }
}
