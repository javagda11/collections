package com.sda.zad4;

public class MainZad4 {
    public static void main(String[] args) {

        System.out.println(dodaj(5, 123.0));
    }

    public static <T extends Number> double dodaj(T pierwsza, T druga){
        return pierwsza.doubleValue() + druga.doubleValue();
    }
}
