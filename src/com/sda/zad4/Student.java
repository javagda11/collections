package com.sda.zad4;

public class Student extends Osoba {
    private long nrIndeksu;

    public Student(String imie, int wiek, long indeks) {
        super(imie, wiek);

        this.nrIndeksu = indeks;
    }

    public long getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(long nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }
}
