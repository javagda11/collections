package com.sda.zad4;

public class Pracownik extends Osoba {
    private double pensja;

    public Pracownik(String imie, int wiek, double pensja) {
        super(imie, wiek);

        this.pensja = pensja;
    }

    public double getPensja() {
        return pensja;
    }

    public void setPensja(double pensja) {
        this.pensja = pensja;
    }
}
