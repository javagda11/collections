package com.sda.zad3;

public class Pudelko<T> {
    private T zawartosc;

    public Pudelko() {
    }

    public Pudelko(T zawartosc) {
        this.zawartosc = zawartosc;
    }

    public T getZawartosc() {
        return zawartosc;
    }

    public void setZawartosc(T zawartosc) {
        this.zawartosc = zawartosc;
    }

    public boolean czyPudelkoJestPuste() {
        if (zawartosc == null) {
            return true;
        } else {
            return false;
        }
    }
}
