package com.sda.collection.studentZad1;

import com.sda.collection.zad5.Plec;

import java.util.ArrayList;

public class Student {
    private String imie, nazwisko, indeks;
    private Plec plec;
    private ArrayList<Double> ocenyWIndeksie;

    public Student(String imie, String nazwisko, String indeks, Plec plec, ArrayList<Double> ocenyWIndeksie) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.indeks = indeks;
        this.plec = plec;
        this.ocenyWIndeksie = ocenyWIndeksie;
    }

    public Student(String imie, String nazwisko, String indeks, Plec plec) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.indeks = indeks;
        this.plec = plec;
    }

    public Student() {
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getIndeks() {
        return indeks;
    }

    public void setIndeks(String indeks) {
        this.indeks = indeks;
    }

    public Plec getPlec() {
        return plec;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }

    public double obliczSredniaStudenta() {
        double suma = 0.0;
        for (Double ocena : ocenyWIndeksie) {
            suma += ocena;
        }
        // srednia
        return suma / ocenyWIndeksie.size();
    }

    public boolean czyStudentZdał() {
        for (Double ocena : ocenyWIndeksie) {
            if (ocena <= 2.0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "Student{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", indeks='" + indeks + '\'' +
                ", plec=" + plec +
                '}';
    }
}
