package com.sda.collection.studentZad1;

import com.sda.collection.zad5.Plec;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student st1 = new Student("marian", "n1", "1231", Plec.KOBIETA, new ArrayList<>(Arrays.asList(3.0, 5.0, 3.5, 4.0)));
        Student st2 = new Student("jola", "n2", "1232", Plec.KOBIETA, new ArrayList<>(Arrays.asList(3.0, 2.0, 3.0, 4.5)));
        Student st3 = new Student("gosia", "n3", "1233", Plec.MEZCZYZNA, new ArrayList<>(Arrays.asList(3.0, 2.0, 3.5, 4.0)));
        Student st4 = new Student("i1", "n4", "1234", Plec.KOBIETA, new ArrayList<>(Arrays.asList(3.0, 2.0, 2.5, 3.0)));
        Student st5 = new Student("i1", "n5", "1235", Plec.MEZCZYZNA, new ArrayList<>(Arrays.asList(3.0, 5.5, 5.5, 4.0)));
        Student st6 = new Student("i1", "n6", "1236", Plec.KOBIETA, new ArrayList<>(Arrays.asList(3.0, 1.5, 1.5, 2.0)));
        Student st7 = new Student("i1", "n7", "1237", Plec.MEZCZYZNA, new ArrayList<>(Arrays.asList(3.0, 3.0, 2.2, 4.0)));

        ArrayList<Student> students = new ArrayList<>();
        students.add(st1);
        students.add(st2);
        students.add(st3);
        students.add(st4);
        students.add(st5);
        students.add(st6);
        students.add(st7);

        // 1
        System.out.println(students);

        // 2
        for (Student student : students) {
            System.out.println(student);
        }

        // 3
        znajdzStudenta(students, null);

        // 4 & 5
        for (Student student : students) {
            System.out.println("Jestem: " + student.getImie() + " i mam srednia: " + student.obliczSredniaStudenta());
        }

        // 6
        for (Student student : students) {
            System.out.println("Jestem: " + student.getImie() + " i zdałem: " + student.czyStudentZdał());
        }

    }

    public static void znajdzStudenta(ArrayList<Student> lista, String szukanyIndeks) {
        for (Student stud : lista) {
//            if (szukanyIndeks.equals(stud.getIndeks())) {
            if (stud.getIndeks().equals(szukanyIndeks)) {
                // jesli odnalazlem studenta
                System.out.println("Odnalazlem: " + stud);
            }
        }
    }
}
