package com.sda.collection.zad3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Tworzymy listę korzystając z Arrays.asList
//        List<Integer> listaNieedytowalna = Arrays.asList(4, 5, 6, 67, 7, 5, 23, 1, 1, 23, 3, 45, 56, 5, 3, 6, 34, 11);

        // kopiujemy wszystkie elementy z listy nieedytowalnej do edytowalnej
//        ArrayList<Integer> edytowalnaKopiaListy = new ArrayList<>(listaNieedytowalna);
//        edytowalnaKopiaListy.add(4);
//        edytowalnaKopiaListy.add(5);
//        edytowalnaKopiaListy.add(6);
//        edytowalnaKopiaListy.add(67);

        // sprawdzenie dodania - błąd
//        listaNieedytowalna.add(55);

        // sprawdzenie dodania - ok!
//        edytowalnaKopiaListy.add(55);

//        System.out.println(edytowalnaKopiaListy);
//        System.out.println(listaNieedytowalna);

        List<Integer> listaIntow = Arrays.asList(10030, 3004, 4000, 12355, 12222, 67888, 111200, 225355, 2222, 1111, 3546, 138751, 235912);
//        if(listaIntow instanceof ArrayList){
//            ///
//            ArrayList<Integer> rzutowana = (ArrayList) listaIntow;
//        }
        ArrayList<String> listaStringow = new ArrayList<>();
        for (Integer wartoscLiczbowa : listaIntow) {
            listaStringow.add(String.valueOf(wartoscLiczbowa));
        }

        //    - określ indeks elementu 138751 posługując się metodą indexOf
        int indeksElementu = listaStringow.indexOf("138751");
        System.out.println("Indeks elementu 138751 to : " + indeksElementu);


        //    - sprawdź czy istnieje na liście obiekt 67888 metodą contains (wynik wypisz na ekran)
        boolean czyJestObiekt67888 = listaStringow.contains("67888");
        System.out.println("Element 67888 znajduje się na liście: " + czyJestObiekt67888);

        boolean czyJestObiekt67889 = listaStringow.contains("67889");
        System.out.println("Element 67889 znajduje się na liście: " + czyJestObiekt67889);

        // usuń z listy obiekt 67888 oraz 67889 (metoda remove)
        boolean powodzenieOperacji = listaStringow.remove("67888");
        System.out.println("Usuniecie 67888: " + powodzenieOperacji);

        powodzenieOperacji = listaStringow.remove("67889");
        System.out.println("Usuniecie 67889: " + powodzenieOperacji);

        // w jednej linii:
        System.out.println(listaStringow);
        // we wielu liniach:
        for (String element : listaStringow) {
            System.out.println(element);
        }
    }
}
