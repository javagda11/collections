package com.sda.collection.zad2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        ArrayList<Integer> orginalnaLista = new ArrayList<>();
        Random generator = new Random();

        //dodaj do listy 10 liczb losowych
        for (int i = 0; i < 10; i++) {
            orginalnaLista.add(generator.nextInt(10));
        }

        //oblicz sumę elementów na liście (wypisz ją)
//        int suma = 0;
//        for (int i = 0; i < orginalnaLista.size(); i++) {
//            suma += orginalnaLista.get(i);
//        }

        int suma = 0;
        for (Integer element : orginalnaLista) {
            suma += element;
        }

        // oblicz średnią elementów na liście (wypisz ją)
        double srednia = suma / orginalnaLista.size();

        // podaj medianę elementów na liście (wypisz ją) (Collections.sort( listaDoPosortowania) - sortuje listę)
        // sortowanie:
        ArrayList<Integer> nowaListaKopia = new ArrayList<>(orginalnaLista);

        Collections.sort(orginalnaLista);
        if (orginalnaLista.size() % 2 == 1) {
            int indeksSrodkowego = orginalnaLista.size() / 2;
            double mediana = orginalnaLista.get(indeksSrodkowego);

            System.out.println(mediana);
        } else {
            int indeksSrodkowego = orginalnaLista.size() / 2;
            double mediana = (orginalnaLista.get(indeksSrodkowego) + orginalnaLista.get(indeksSrodkowego - 1)) / 2.0;

            System.out.println(mediana);
        }

        /// praca na kopii - tej która nie jest posortowana

//        int minimum = tablica[0];
//        int maximum = tablica[0];
//        for (int i = 1; i < tablica.length; i++) {
//            if (minimum > tablica[i]) {
//                minimum = tablica[i];
//            }
//            if (maximum < tablica[i]) {
//                maximum = tablica[i];
//            }
//        }
//        System.out.println(minimum);
//        System.out.println(maximum);

        int minimum = nowaListaKopia.get(0);
        int maximum = nowaListaKopia.get(0);
        for (int i = 1; i < nowaListaKopia.size(); i++) {
            if (minimum > nowaListaKopia.get(i)) {
                minimum = nowaListaKopia.get(i);
            }
            if (maximum < nowaListaKopia.get(i)) {
                maximum = nowaListaKopia.get(i);
            }
        }
        System.out.println("Minimum: " + minimum);
        System.out.println("Maximum: " + maximum);

        ////////// znajdź największy oraz najmniejszy element na liście (znajdź go pętlą for, a następnie określ index posługując się metodą indexOf)

        int indexMaximum = 0;
        int indexMinimum = 0;

        for (int i = 0; i < nowaListaKopia.size(); i++) {
            if (nowaListaKopia.get(i) == maximum) {
                indexMaximum = i;
            }
            if (nowaListaKopia.get(i) == minimum) {
                indexMinimum = i;
            }
        }

        // znalezienie metodami
//        int indexMaximum = nowaListaKopia.indexOf(maximum);
//        int indexMinimum = nowaListaKopia.indexOf(minimum);
        System.out.println("Index max: " + indexMaximum);
        System.out.println("Index min: " + indexMinimum);


//        int[] tablica = new int[10];
//        int[] inna = new int[tablica?];
    }
}
