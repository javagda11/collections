package com.sda.collection.parkingZad2;

import java.util.ArrayList;

public class Parking {
    private ArrayList<MiejsceParkingowe> miejscaParkingowe;
    private boolean czyJestOtwarty; // jesli true to otwarty

    public Parking() {
    }

    public Parking(ArrayList<MiejsceParkingowe> miejscaParkingowe, boolean czyJestOtwarty) {
        this.miejscaParkingowe = miejscaParkingowe;
        this.czyJestOtwarty = czyJestOtwarty;
    }

    // sprawdźZajętośćMiejsca(int numerMiejsca):boolean
    public boolean sprawdźZajętośćMiejsca(int numerMiejsca) {
        // iteruję przez wszystkie elementy na liście miejsc parkingowych
        for (MiejsceParkingowe miejsce : miejscaParkingowe) {
            // dla każdego miejsca parkingowego sprawdzam, czy jego numer to numerMiejsca (szukany)
            if (miejsce.getNumerMiejsca() == numerMiejsca) {
                // jeśli tak, to sprawdzam czy miejsce jest zajęte, porównując jego stan

                //return miejsce.getStan() == StanMiejsca.WOLNE;
                // równe
                if (miejsce.getStan() == StanMiejsca.WOLNE) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        // jeśli nie udało mi się odnaleźć miejsca, to zwracam false - miejsce nie jest wolne (bo nie istnieje)
        return false;
    }


    // zajmijMiejsce(int numerMiejsca):boolean
    public boolean zajmijMiejsce(int numerMiejsca) {
        for (MiejsceParkingowe miejsce : miejscaParkingowe) {
            // dla każdego miejsca parkingowego sprawdzam, czy jego numer to numerMiejsca (szukany)
            if (miejsce.getNumerMiejsca() == numerMiejsca) {
                // znalazlem szukane miejsce
                if (miejsce.getStan() == StanMiejsca.ZAJETE) {
                    // jesli miejsce jest zajete to nie mozemy go ponownie zajac
                    // zwracam false - bo nie udalo mi sie zjac tego miejsca.
                    return false;
                } else {
                    // jesli miejsce jest wolne (przeciwny przypadek == ZAJETE) to oznaczam je jako zajete
                    miejsce.setStan(StanMiejsca.ZAJETE);
                    // zwracam true bo udalo mi sie je zajac
                    return true;
                }
            }
        }
        // jeśli nie udało mi się odnaleźć miejsca, to zwracam false - miejsce nie moze byc zajete (bo nie istnieje)
        return false;
    }

    // Druga opcja:
    // zajmijMiejsce(int numerMiejsca):boolean
    public boolean zajmijMiejsce2(int numerMiejsca) {
        boolean czyMiejsceJestWolne = sprawdźZajętośćMiejsca(numerMiejsca);
        if(czyMiejsceJestWolne){
            for (MiejsceParkingowe miejsce : miejscaParkingowe) {
                // dla każdego miejsca parkingowego sprawdzam, czy jego numer to numerMiejsca (szukany)
                if (miejsce.getNumerMiejsca() == numerMiejsca) {
                    miejsce.setStan(StanMiejsca.ZAJETE);
                    return true;
                }
            }
        }

        // jeśli nie udało mi się odnaleźć miejsca, to zwracam false - miejsce nie moze byc zajete (bo nie istnieje)
        return false;
    }
}
