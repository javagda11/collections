package com.sda.collection.parkingZad2;

public class MiejsceParkingowe {
    private int numerMiejsca;
    private StanMiejsca stan;

    public MiejsceParkingowe() {
    }

    public MiejsceParkingowe(int numerMiejsca, StanMiejsca stan) {
        this.numerMiejsca = numerMiejsca;
        this.stan = stan;
    }

    public int getNumerMiejsca() {
        return numerMiejsca;
    }

    public void setNumerMiejsca(int numerMiejsca) {
        this.numerMiejsca = numerMiejsca;
    }

    public StanMiejsca getStan() {
        return stan;
    }

    public void setStan(StanMiejsca stan) {
        this.stan = stan;
    }

    @Override
    public String toString() {
        return "MiejsceParkingowe{" +
                "numerMiejsca=" + numerMiejsca +
                ", stan=" + stan +
                '}';
    }
}
