package com.sda.collection.zad5;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Student marian = new Student("123", "Marina", "Marianski", Plec.MEZCZYZNA);
        Student maniek = new Student("124", "Maniek", "Kowalski", Plec.MEZCZYZNA);
        Student jola = new Student("125", "jola", "Nowak", Plec.KOBIETA);

        ArrayList<Student> studenci = new ArrayList<>();
        studenci.add(maniek);
        studenci.add(marian);
        studenci.add(jola);

        //a. Spróbuj wypisać elementy listy stosując zwykłego "sout'a"
        System.out.println(studenci);

        //b. Spróbuj wypisać elementy stosując pętlę foreach
        for (Student student : studenci) {
            System.out.println(student);
        }

        //c. Wypisz tylko kobiety
        for (Student student : studenci) {
            if (student.getPlec() == Plec.KOBIETA) {
                // jeśli jest kobietą to wypisz
                System.out.println(student);
            }
            // w przeciwnym razie nic.
        }

        //d. Wypisz tylko mężczyzn
        for (Student student : studenci) {
            if (student.getPlec() == Plec.MEZCZYZNA) {
                // jeśli jest mężczyzną to wypisz
                System.out.println(student);
            }
            // w przeciwnym razie nic.
        }

        //e. Wypisz tylko indeksy osób
        for (Student student : studenci) {
            System.out.println(student.getIndeks());
        }


//        Object zmienna = 23 + "";
////        Object zmienna = String.valueOf(23);
//
//        if(zmienna instanceof Integer){
//            System.out.println("int");
//        }else if(zmienna instanceof String){
//            System.out.println("string");
//        }

    }
}
