package generycznie;

public class Para <T, W> {
    private T lewy;
    private W prawy;

    public Para(T lewy, W prawy) {
        this.lewy = lewy;
        this.prawy = prawy;
    }

    public T getLewy() {
        return lewy;
    }

    public void setLewy(T lewy) {
        this.lewy = lewy;
    }

    public W getPrawy() {
        return prawy;
    }

    public void setPrawy(W prawy) {
        this.prawy = prawy;
    }
}
